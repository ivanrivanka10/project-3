package com.example.tugas3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    BottomNavigationView navigationView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        navigationView = findViewById(R.id.view);
        navigationView.setOnNavigationItemSelectedListener(this);
        loadFragment(new HomeFragment());

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_home:
                loadFragment(new HomeFragment());
                break;
            case R.id.menu_profile:
                loadFragment(new ProfileFragment());
                break;

            case R.id.menu_laptop :
                loadFragment(new LaptopFragment());
                break;
            case R.id.menu_youtube :
                loadFragment(new YoutubeFragment());
                break;


        }
        return true;
    }

    private void loadFragment(Fragment selected){
        if(selected!=null){

            getSupportFragmentManager().beginTransaction().replace(R.id.frame,selected).show(selected).commit();
        }

    }
}
